<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class CommentController extends Controller
{
    // public function saveComment(Request $request, $id){
    //    $post = Post::find($id);

    //    if($post->user_id != Auth::user()->id){
    //     $postComment = new PostComment;
    //     $postComment->post_id = $post->id;
    //     $postComment->content = $request->input('content');
    //     $postComment->user_id = Auth::user()->id;

    //     $postComment->save();
    //    }

    // //    return redirect("/posts/$id");
    // return redirect("/posts");
        
    // }

     public function saveComment(Request $request, $id){
       
        $post = Post::find($id);

        if(Auth::user()){
            
        $post_comment = new PostComment;
        $post_comment->content=$request->input('content');
        $post_comment->user_id=(Auth::user()->id);

        $post_comment->save();

        return redirect ('/posts/{id}/comment');

        } else {
            return redirect('/posts/{id}');
        }
    
    }

    // public function saveComment(Request $request, Post $post)
    // {
    //     $validatedData = $request->validate([
    //         'content' => 'required|string|max:255',
    //     ]);

    //     // Create a new comment associated with the post and the authenticated user
    //     $comment = new PostComment();
    //     $comment->content = $validatedData['content'];
    //     $comment->user_id = auth()->user()->id;
    //     $comment->post_id = $post->id;
    //     $comment->save();

    //     return redirect()->back()->with('success', 'Comment added successfully.');
    // }
}
