<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [PostController::class,'welcome']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// This route is for creation of a new post:
Route::get('/posts/create', [PostController::class,'createPost']);

// This route is for the list of post on our database:
Route::get('/posts', [PostController::class,'showPosts']);

// This route is for saving post in our database
Route::post('/posts', [PostController::class, 'savePost']);

// This route is for logging out
Route::get('/logout', [PostController::class,'logout']);

// COMMENT

// Route::post('/posts/{id}/comment', [CommentController::class, 'saveComment']);

Route::post('/posts/{id}/comment', [PostController::class, 'saveComment']);

//  END COMMENT


Route::get('/myPosts',[PostController::class,'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user

Route::get('/posts/{id}', [PostController::class,'show']);

// Define a route that will return an edit form

Route::get('/posts/{id}/edit', [PostController::class, 'editForm'])->name('posts.editForm');

//define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

//define a route that will delete a post of the matching URL parameter ID
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// Define a web route that will call the function for liking and unliking a specific post

Route::put('/posts/{id}/like', [PostController::class,'like']);



