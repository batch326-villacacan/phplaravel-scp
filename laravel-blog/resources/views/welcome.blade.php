@extends('layouts.app')

@section('content')
<img src = "https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class = "w-25 mx-auto d-block img-fluid my-5"/>
<h3 class = "text-center">Featured Posts:</h3>
    @if(count($posts)>0)
        @foreach($posts as $post)
           <div class = "card text-center col-6 mx-auto mt-2">
                <div class = "card-body flex justify-center">
                    <h4 class = "card-title mb-3">
                        <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                    </h4>
                    <!-- built-in si 'user' -->
                    <h6 class = "card-text mb-3">Author: {{$post->user->name}}</h6>
                </div>           
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class = "btn btn-info">Create Post</a>
        </div>
    @endif

@endsection