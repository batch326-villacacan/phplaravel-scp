@extends('layouts.app')

@section('content')
    <form class = "col-8 bg-secondary p-5 mx-auto" method = "POST" action = "/posts">
        
        <!-- CSRF stands for Cross-Site Request Forgery.
    It is a form of attack where malicious users may send malicious request while pretending to be authorize user. Laravel uses token to detect if form input request have not been tampered with. -->

        @csrf

        <div class = "form-group">
            <label for = 'title' class = "text-white">Title:</label>
            <input type="text" name="title" class="form-control" id="title" />
        </div>

        <div class = "form-group">
            <label for = "content" class = "text-white">Content:</label>
            <textarea class = "form-control" id = "content" name="content"  rows = 3></textarea>
        </div>

        <div class = "mx-auto mt-4">
            <button class = "btn btn-primary">Create Post</button>
        </div>
    </form>
@endsection