@extends('layouts.app')

@section('content')
    <div class = 'card col-8 mx-auto'>
    <h2 class = 'card-title'>{{$post->title}}</h2>
                <p class = 'card-subtitle text-muted'>Author: {{$post->user->name}}</p>
                <p class = 'card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
                <p class = 'card-text'>{{$post->body}}</p>
    </div>
@endsection