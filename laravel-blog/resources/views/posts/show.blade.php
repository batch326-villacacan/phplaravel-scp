@extends('layouts.app')

@section('tabName')
	{{$post->title}}
@endsection

@section('content')
    <div class = 'card col-8 mx-auto'>
            <div class = 'card-body'>
                <h2 class = 'card-title'>{{$post->title}}</h2>
                <p class = 'card-subtitle text-muted'>Author: {{$post->user->name}}</p>
                <p class = 'card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
                <p class = 'card-text'>{{$post->body}}</p>

                <p class="card-text text-muted mb-4">Likes: {{count($post->likes)}} | Comments: {{count($post->comments)}}</p>

                <!-- LIKE -->
                @if(Auth::user())
                    @if(Auth::id() != $post->user_id)
                        <form class = "d-inline" method = "POST" action="/posts/{{$post->id}}/like">
                            @method('PUT')
                            @csrf
                                @if($post->likes->contains('user_id', Auth::id()))
                                    <button class = "btn btn-danger">Unlike</button>
                                @else
                                    <button class = "btn btn-success">Like</button>
                                @endif
                        </form>
                    @endif

                <!-- comment -->
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Comment
                    </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">Leave a comment</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        
                        <form class = "d-inline" method = "POST" action="/posts/{{$post->id}}/comment">
                        @csrf
                            <div class="modal-body mb-3">
                                
                                    <label for="content" class="col-form-label">Comment:</label>
                                    <textarea class="form-control" id="content" name="content" rows = 3></textarea>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Post Comment</button>
                            </div>
                        </form>
                        
                        
                        </div>
                    </div>
                    </div>
                @endif

                <a href="/posts" class = "btn btn-success text-white" >View all posts</a>
            </div>

            
    </div>

    <h4>Comments:</h4>

    @if(count($post->comments)>0)
		@foreach($post->comments as $comments)

        <div class="card">
            <div class="card-body">
                <p class="card-text">{{$comments->content}}</p>
                
                <p class="text-muted mt-5" style="line-height: 2px; margin-left: 65rem; font-size: 0.8rem;">commented by: {{$comments->user->name}}</p>
				
                <p class="text-muted mb-1" style="line-height: 2px; margin-left: 65rem; font-size: 0.8rem;">commented on: {{$comments->created_at}}</p>
            </div>

        </div>


        @endforeach

        @else
		<div>
			<h2>There are no comments to show</h2>
		</div>
	@endif

@endsection

